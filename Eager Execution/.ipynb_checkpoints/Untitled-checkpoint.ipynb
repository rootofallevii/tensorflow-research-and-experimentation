{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TensorFlow - Eager Execution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "tf.enable_eager_execution()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tensors\n",
    "\n",
    "A **tensor** is a multi-dimensional array. Similar to NumPy `ndarray` objects, `Tensor` objects have a data type and a shape. Additionally, Tensors can reside in accelerator (like GPU) memory. TensorFlow offers a rich library of operations that consume and produce Tensors. These operations automatically convert native Python types.\n",
    "\n",
    "The most obvious differences between NumPy arrays and TensorFlow Tensors are:\n",
    "1. Tensors can be backed by accelerated memory (GPU, TPU),\n",
    "2. Tensors are immutable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor(3, shape=(), dtype=int32)\n",
      "tf.Tensor([4 6], shape=(2,), dtype=int32)\n",
      "tf.Tensor(25, shape=(), dtype=int32)\n",
      "tf.Tensor(6, shape=(), dtype=int32)\n",
      "tf.Tensor(b'aGVsbG8gd29ybGQ', shape=(), dtype=string)\n"
     ]
    }
   ],
   "source": [
    "print(tf.add(1, 2))\n",
    "print(tf.add([1, 2], [3, 4]))\n",
    "print(tf.square(5))\n",
    "print(tf.reduce_sum([1, 2, 3]))\n",
    "print(tf.encode_base64(\"hello world\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor(13, shape=(), dtype=int32)\n"
     ]
    }
   ],
   "source": [
    "print(tf.square(2) + tf.square(3)) # Operator overloading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor([[ 8 12]], shape=(1, 2), dtype=int32)\n",
      "(1, 2)\n",
      "<dtype: 'int32'>\n"
     ]
    }
   ],
   "source": [
    "x = tf.matmul([[4]], [[2, 3]])\n",
    "print(x)\n",
    "print(x.shape)\n",
    "print(x.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### NumPy Compatibility\n",
    "\n",
    "TensorFlow and NumPy operations first convert the argument to the appropriate type (from `ndarray` to `Tensor`, or the other way around) before carrying out the operation.\n",
    "\n",
    "Tensors can be explicitly converted to NumPy `ndarray`s by invoking the `.numpy()` method on them. Even though `Tensor`s and `ndarray`s have the same memory representation, conversion may require copy memory from GPU to main memory and thus can be expensive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor(\n",
      "[[42. 42. 42.]\n",
      " [42. 42. 42.]\n",
      " [42. 42. 42.]], shape=(3, 3), dtype=float64)\n",
      "[[43. 43. 43.]\n",
      " [43. 43. 43.]\n",
      " [43. 43. 43.]]\n",
      "[[42. 42. 42.]\n",
      " [42. 42. 42.]\n",
      " [42. 42. 42.]]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "ndarray = np.ones([3, 3])\n",
    "tensor = tf.multiply(ndarray, 42)\n",
    "print(tensor) # First convert ndarray to tensor then multiply\n",
    "\n",
    "print(np.add(tensor, 1)) # First convert tensor to ndarray then add\n",
    "\n",
    "print(tensor.numpy()) # Explicitly convert tensor to ndarray"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### GPU Acceleration\n",
    "\n",
    "Without any annotations, TensorFlow automatically decides whether to use the GPU or CPU for an acceleration (and copies the tensor between GPU and CPU memory if necessary). Tensors produced by an operation are typically backed by the memory of the device on which the operation executed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "x = tf.random_uniform([3, 3])\n",
    "\n",
    "print(tf.test.is_gpu_available())\n",
    "print(x.device.endswith('GPU:0'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Tensor.device` property provides a fully qualified string name of the device hosting the contents of the tensor. The string ends with `GPU:<N>` if the tensor is placed on the `N`-th GPU on the host."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Explicit Device Placement\n",
    "\n",
    "The term *placement* in TF refers to how individual operations are assigned (placed on) a device for execution. If no expliclty guidance is provided, TF automatically decides which device to use. You can also place an operation on one device by calling `tf.device` context manager:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "On CPU:\n"
     ]
    },
    {
     "ename": "AttributeError",
     "evalue": "'tensorflow.python.framework.ops.EagerTensor' object has no attribute 'tf'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-fbe20c0d32e8>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"On CPU:\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      6\u001b[0m \u001b[0;32mwith\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdevice\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"CPU:0\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 7\u001b[0;31m     \u001b[0mx\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrandom_uniform\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m1000\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m1000\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      8\u001b[0m     \u001b[0;32massert\u001b[0m \u001b[0mxdevice\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mendswith\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"CPU:0\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      9\u001b[0m     \u001b[0mtime_matmul\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mx\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'tensorflow.python.framework.ops.EagerTensor' object has no attribute 'tf'"
     ]
    }
   ],
   "source": [
    "def time_matmul(x):\n",
    "    %timeit tf.matmul(x, x)\n",
    "    \n",
    "# CPU Execution\n",
    "print(\"On CPU:\")\n",
    "with tf.device(\"CPU:0\"):\n",
    "    x.tf.random_uniform([1000, 1000])\n",
    "    assert xdevice.endswith(\"CPU:0\")\n",
    "    time_matmul(x)\n",
    "    \n",
    "# GPU Execution\n",
    "if tf.test.is_gpu_available():\n",
    "    with tf.device(\"GPU:0\"):\n",
    "        x.tf.random_uniform([1000, 1000])\n",
    "        assert xdevice.endswith(\"GPU:0\")\n",
    "        time_matmul(x)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
